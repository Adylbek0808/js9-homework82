const path = require('path');
const rootPath = __dirname;

module.exports = {
    rootPath,
    artistsUploadPath: path.join(rootPath, 'public/uploads/artists'),
    albumsUploadPath: path.join(rootPath, 'public/uploads/albums')
}