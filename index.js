const express = require('express');
const cors = require("cors");
const exitHook = require('async-exit-hook');
const mongoose = require('mongoose');

const artists = require('./app/artists');
const albums = require ('./app/albums');
const tracks = require('./app/tracks')

const app = express();
app.use(express.static('public'));
app.use(express.json());
app.use(cors());

const port = 8000


app.use('/artists', artists);
app.use('/albums', albums);
app.use('/tracks', tracks)


const run = async () => {
    await mongoose.connect('mongodb://localhost/radio', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });

    exitHook(async callback => {
        console.log('exiting');
        await mongoose.disconnect();
        callback();
    });

}

run().catch(console.error)